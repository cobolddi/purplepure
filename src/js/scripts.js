jQuery(function($){
  'use strict'

AOS.init({
mirror: false,
once:true,
easing: 'ease-in-out'
});
  $(document).ready(function() {
    let ES6 = x => console.log("ES6 Running");
    ES6();
    let Page_Height = $(document).height();
    console.log("Page_Height", Page_Height);
    let WindowWidth = $( window ).width();
    console.log("window_width", WindowWidth);


    // var headerHeight = $('#header').outerHeight(true);
    // console.log("Header Height", headerHeight);
    // $('main').css('paddingTop', headerHeight);

    // Header Fixed on offset top
    var stickyOffset = $('#header').offset().top;
  $(window).scroll(function(){
    var sticky = $('#header'),
        scroll = $(window).scrollTop();

    if (scroll >= stickyOffset) sticky.addClass('fixed');
    else sticky.removeClass('fixed');
  });

   // Window offset top up on click on arrow
   var body = $('html, body');
   var bannerHeight = $("#homeBanner").height();
   console.log("banner_height" , bannerHeight);
   $("#scroll_down").on("click", function(){
     body.animate({"scrollTop": window.scrollY+bannerHeight}, 100);
        return false;
   });


  // Section Scroll on click on nav links
  function scrollNav() {
    $('.NavContainer ul li>a').click(function () {
      //Toggle Class
      $(".active").removeClass("active");
      $(this).closest('li').addClass("active");
      var theClass = $(this).attr("class");
      $('.' + theClass).parent('li').addClass('active'); //Animate

      $('html, body').stop().animate({
        scrollTop: $($(this).attr('href')).offset().top - 75    
      }, 100);
      return false;
    });
    $('.scrollTop a').scrollTop();
  }
  scrollNav();

// Expore the science and Browse Product Scroll to Section
function scrollNav1() {
    $('#aboutUs .contentWrap .btn-group>a').click(function () {
      $(".active").removeClass("active");
      $(this).closest('li').addClass("active");
      var theClass = $(this).attr("class");
      $('.' + theClass).parent('li').addClass('active'); //Animate

      $('html, body').stop().animate({
        scrollTop: $($(this).attr('href')).offset().top - 75
      }, 200);
      return false;
    });
    $('.scrollTop a').scrollTop();
  }

  scrollNav1();


// Join the wishlist form validation
  $(".contactform").validate({
    rules: {
      fName: "required",
      // lName: "required",

       Phone: {
        required: true,
        minlength: 10,
        maxlength: 10
      },

      Email: {
        required: true,
        email: true
      },

      City:{
       required: true
      }
 
      // Message: {
      //   required: true
      // }
    },
    messages: {
      fName: "Please enter your firstname",
      // lName: "Please enter your lastname",
      Phone: {
        required: "Please provide a phone number",
        minlength: "Please provide a valid phone number"
      },
      Email: {
        required: "Please enter a email address",
        email: "Please enter a valid email address"
      },

      City: "Please enter your City Name"
      // Message: "Message field is required"
    },
    submitHandler: function submitHandler(form) {
      $.ajax({
        // down: ""
        url: "mail.php",
        // dataType: "JSON",
        type: "POST",
        data: $(form).serialize(),
        beforeSend: function beforeSend(xhr) {
          $('#submit').val('SENDING...');
        },
        success: function success(response) {
          if (response) {
            console.log(response, "Success");
            $('#submit').val('Send Message');
            $('.contactform').append('<div class="success FormMessage" id="successMsg">Thanks for contacting us. We will get back to you soon!</div>');
            $(form)[0].reset();
            
          }
        },
        error: function error(err) {
          $('#submit').val('Failed');
          console.log(err, "error");
          $('.contactform').append('<div class="error FormMessage">Unable to submit form. Please try again!</div>');
          $(form)[0].reset();
        }
      });
    }
  });

// Success message hide after submmission the form
setTimeout(function() {
$('.contactform #successMsg').hide();
}, 5000);

  // Download PDf form validation
  $(".pdfForm").validate({
    rules: {
      fName: "required",
      // lName: "required",

       Phone: {
        required: true,
        minlength: 10,
        maxlength: 10
      },

      Email: {
        required: true,
        email: true
      },

      City:{
       required: true
      }
 
      // Message: {
      //   required: true
      // }
    },
    messages: {
      fName: "Please enter your firstname",
      // lName: "Please enter your lastname",
      Phone: {
        required: "Please provide a phone number",
        minlength: "Please provide a valid phone number"
      },
      Email: {
        required: "Please enter a email address",
        email: "Please enter a valid email address"
      },

      City: "Please enter your City Name"
      // Message: "Message field is required"
    },
    submitHandler: function submitHandler(form) {
      $.ajax({
        url: "mail.php",
        // dataType: "JSON",
        type: "POST",
        data: $(form).serialize(),
        beforeSend: function beforeSend(xhr) {
          $('#submit').val('SENDING...');
        },
        success: function success(response) {
          if (response) {
            console.log(response, "Success");
            $('#submit').val('Send Message');
            $('.contactform').append('<div class="success FormMessage">Thanks for contacting us. We will get back to you soon!</div>');
            $(form)[0].reset();
            window.open('pdf/IUVA_Fact_Sheet_on_COVID_19.pdf', '_blank');
            // ga('send', 'event', 'Form', 'Contact Form', 'Contact Form Submission', {
            //   useBeacon: true
            // });
          }
        },
        error: function error(err) {
          $('#submit').val('Failed');
          console.log(err, "error");
          $('.contactform').append('<div class="error FormMessage">Unable to submit form. Please try again!</div>');
          $(form)[0].reset();
        }
      });
    }
  });


    
    
// Launch Date Timer
function makeTimer() {
    var endTime = new Date("14 June 2020 9:56:00 GMT+01:00");      
      endTime = (Date.parse(endTime) / 1000);

      var now = new Date();
      now = (Date.parse(now) / 1000);

      var timeLeft = endTime - now;

      var days = Math.floor(timeLeft / 86400); 
      var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
      var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
      var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));
  
      if (hours < "10") { hours = "0" + hours; }
      if (minutes < "10") { minutes = "0" + minutes; }
      if (seconds < "10") { seconds = "0" + seconds; }

      $("#days").html(days + "<span>DAYS</span>");
      $("#hours").html(hours + "<span>HOURS</span>");
      $("#minutes").html(minutes + "<span>MINUTES</span>");
      $("#seconds").html(seconds + "<span>SECONDS</span>");   

  }

  setInterval(function() { makeTimer(); }, 1000);


    //Mobile menu
    $(".MenuBar").click(function(e){
      e.preventDefault();
      $("#header .NavContainer").addClass("Active_Nav");
    });
    $(".MenuOverlay").click(function(e){
      e.preventDefault();
      $("#header .NavContainer").removeClass("Active_Nav");
    });
  
  // Navbar Drawer close on click 
  $(".NavContainer ul>li a").click(function(){
     $("#header .NavContainer").removeClass("Active_Nav");
  });

    //Check Screen size
    let checkScrennSize = () => {
      if (window.matchMedia('(max-width: 991px)').matches) {
          // $(".haveSubmenu>a").append('<span><img src="assets/img/chevron-down-white.svg"></span>');
          $(".haveSubmenu>a").append('<span></span>');
      }
      $(".haveSubmenu").find('a>span').click(function(e){
      // $(".haveSubmenu>a>span").click(function(e){
        // e.stopPropagation();
        e.preventDefault();
        $(this).parents('a').parent().toggleClass("Active_MobileDropdown");
      });
    }
    checkScrennSize();

   
  //Download Report form
 $('.downloadReport').magnificPopup({
      type: 'inline',
      mainClass: 'mfp-fade',
      removalDelay: 100,
      preloader: true,
      fixedContentPos: false
  });

    //Gallery Slider
    $(".productSlider").slick({
      dots:true,
      autoplay: false,
      autoplaySpeed: 3000,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      pauseOnHover: true,
      slidesToScroll: 1
    });

    //Card Slider
    $(".SliderWrap").slick({
      dots: false,
      arrow: true,
      autoplay: true,
      autoplaySpeed: 3000,
      infinite: true,
      pauseOnHover: true,
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 1,
      responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          // adaptiveHeight: true
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          // adaptiveHeight: true
        }
      }
    ]
    });
  
  }); //--End document.ready();
});

  