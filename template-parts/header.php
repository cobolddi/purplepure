<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="PurplePure keeping our new warriors against corona deases">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="icon" type="image/x-icon" href="assets/img/favicon.png">
	<title>PurplePure</title>
    <link rel="stylesheet" href="assets/css/vendor.min.css">
    <link rel="stylesheet" href="assets/css/styles.min.css">
    <!--[if lt IE 9]>
     <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->
</head>
<body>
	<?php include('template-parts/banner.php'); ?>
	<header class="Header" id="header">
		<div class="container">
			<div class="MenuContainer">
				<div class="LogoContainer">
					<a href="index.php"><img src="assets/img/tempimg/logo_navigation.svg" alt="logo_navigation" width="184px" height="25px"></a>
				</div>
				<div class="NavContainer">
					<nav>
						<div class="menu-main-menu-container">
							<ul>
								<li><a href="#aboutUs">About PurplePure</a></li>
								<li><a href="#disinfectionScience">UVC Disinfection Science</a></li>
								<li><a href="#applications">Applications</a></li>
								<li><a href="#products">Products</a></li>
								<li><a href="#investors">Resources</a></li>
								<li><a href="#contactUs">Contact Us</a></li>
								<!-- <li class="MenuButton"><a href="#">Appointments</a></li> -->
							</ul>
						</div>
					</nav>
					<div class="MenuOverlay MobileNavController">
						<a class="CloseMenuIcon MobileNavController" href="#">
							<span>&times;</span>
						</a>
					</div>
					<a class="MenuBar MobileNavController" href="#">
						<div>
							<span></span>
							<span></span>
							<span></span>
						</div>
					</a>
				</div>
			</div>
		</div>
	</header>
	<main>
	

	