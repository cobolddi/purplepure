
    </main>
    <footer>
        <div class="container">
            <div class="footer_logo">
                <a href="#">
                    <img src="assets/img/tempimg/medium_logo.svg" alt="logo" width="144px" height="auto">
                </a>
            </div>
            <div class="addressInfo">
                <span>KH No. 403, Ghitorni, Main MG Road, Near Metro Pillar 118-119, New Delhi</span>
            </div>
            <div class="mobileOnly">
                <div class="follow_us">
                    <span>Follow Us</span>
                    <ul class="SocialIcons"> 
                        <li>
                            <a href="#" target="_blank">
                                <svg>
                                    <use xlink:href="assets/img/cobold-sprite.svg#facebook-icon"></use>
                                </svg>
                            </a>
                        </li>
                        
                        <li>
                            <a href="#" target="_blank">
                                <svg>
                                    <use xlink:href="assets/img/cobold-sprite.svg#instagram-icon"></use>
                                </svg>
                            </a>
                        </li>
                        
                        <li>
                            <a href="#" target="_blank">
                                <svg>
                                    <use xlink:href="assets/img/cobold-sprite.svg#linkedin-icon"></use>
                                </svg>
                            </a>
                        </li>
                                                        
                        <li>
                            <a href="#" target="_blank">
                                <svg>
                                    <use xlink:href="assets/img/cobold-sprite.svg#icon-twitter"></use>
                                </svg>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="FooterBottom">
            <div class="container">
                <div class="row alignItemCenter">
                    <div class="col-lg-4 col-md-6 text-left">
                        Copyright <script>document.write(new Date().getFullYear()) </script>, PurplePure
                    </div>
                    <div class="col-lg-4 text-center">
                        <div class="desktopOnly">
                            <div class="follow_us">
                                <span>Follow Us</span>
                                <ul class="SocialIcons"> 
                                    <li>
                                        <a href="#" target="_blank">
                                            <svg>
                                                <use xlink:href="assets/img/cobold-sprite.svg#facebook-icon"></use>
                                            </svg>
                                        </a>
                                    </li>
                                    
                                    <li>
                                        <a href="#" target="_blank">
                                            <svg>
                                                <use xlink:href="assets/img/cobold-sprite.svg#instagram-icon"></use>
                                            </svg>
                                        </a>
                                    </li>
                                    
                                    <li>
                                        <a href="#" target="_blank">
                                            <svg>
                                                <use xlink:href="assets/img/cobold-sprite.svg#linkedin-icon"></use>
                                            </svg>
                                        </a>
                                    </li>
                                                                    
                                    <li>
                                        <a href="#" target="_blank">
                                            <svg>
                                                <use xlink:href="assets/img/cobold-sprite.svg#icon-twitter"></use>
                                            </svg>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 text-right">
                        <span>
                            Delivered by <a href="http://cobold.co/" target="_blank">Cobold Digital</a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </footer>

 <!-- Download Report Form -->
    <div class="reportWrapper mfp-hide" id="reportWrap">
        <h2>Report on UVA disinfection for your home</h2>
      <div class="formWrapper">
            <form action="" method="post" class="contactform pdfForm">
                <input type="text" placeholder="First Name*" name="fName">
                <input type="text" placeholder="Last Name" name="lName">
                <input type="tel" placeholder="Phone Number*" name="Phone">
                <input type="email" placeholder="Email*" name="Email">
                <input type="text" placeholder="City*" name="City">
                <!-- <div class="input-group">
                    <label for="message">Message</label>
                    <textarea name="Message" id="message"></textarea>
                </div> -->
                <input type="submit" value="Submit" id="submit">
            </form>
        </div>
        <button title="Close(Esc)" type="button" class="mfp-close">&times;</button>
    </div>
    <!--/footer-->
</div>
<!-- page -->
      
    <div id="toTop"></div><!-- Back to top button -->
    <script src="assets/js/vendor.js"></script>
    <script src="assets/js/scripts.js"></script>
</body>
</html>