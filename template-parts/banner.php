<section class="HomeBanner" id="homeBanner">
    <div class="bannerWrapper">
        <!-- <picture>
            <source media="(min-width: 768px)" srcset="assets/img/tempimg/person-wearing-blue-sterile-gloves.png">
            <img src="assets/img/mobileBanner.png" alt="banner_image">
        </picture> -->
        <div class="imgWrap">
        <img src="assets/img/tempimg/person-wearing-blue-sterile-gloves.png" alt="banner_image" width="100%" height="100%">
        </div>
        <div class="bannerContent">
            <div class="container">
            <a href="#" data-aos="zoom-in" data-aos-duration="1000" data-aos-offset="0"><img src="assets/img/tempimg/large_logo.svg" alt="logo" width="311px" height="318px"></a>
            <h1 data-aos="fade-up" data-aos-duration="1000" data-aos-offset="0">Scientific Sterilization</h1>
            </div>
        </div>
    </div>
    <div class="scrollDown" id="scroll_down">
        <svg class="icon icon-arrow_down">
            <use xlink:href="assets/img/cobold-sprite.svg#icon-arrow_down"></use>
        </svg>
    </div>
</section>