<section class="AccordionSection">
    <div class="container">
        <div class="wrap">
            <ul class="accordion">
                <li class="accordion__item">
                    <a class="accordion__title" href="">Care and maintenance
                        <div class="bmenu x7">
                            <span class="btop"></span><span class="bbot"></span>
                        </div>
                    </a>
                    <div class="accordion__content">
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                        </p>
                        <h3 class="Head">Heading</h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent congue tincidunt tempor. Sed vitae neque libero. In efficitur ex eget nunc vestibulum, eu molestie metus condimentum. Integer non elit et orci scelerisque egestas. Suspendisse a semper nisl. Sed consequat velit ac aliquet facilisis. Donec molestie tellus id suscipit tincidunt. Mauris ac mauris urna. Quisque vitae eros sodales, tempor ex non, pellentesque quam. Etiam aliquet sem ac luctus venenatis. Sed faucibus velit in tellus ullamcorper, fringilla maximus purus tempus. Cras semper mauris eget fermentum porttitor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent congue tincidunt tempor. Sed vitae neque libero. In efficitur ex eget nunc vestibulum, eu molestie metus condimentum. Integer non elit et orci scelerisque egestas. Suspendisse a semper nisl. Sed consequat velit ac aliquet facilisis. Donec molestie tellus id suscipit tincidunt. Mauris ac mauris urna. Quisque vitae eros sodales, tempor ex non, pellentesque quam. Etiam aliquet sem ac luctus venenatis. Sed faucibus velit in tellus ullamcorper, fringilla maximus purus tempus. Cras semper mauris eget fermentum porttitor.
                        </p>
                    </div>
                </li>
                <li class="accordion__item">
                    <a class="accordion__title" href="">Lorem ipsum dolor sit
                        <div class="bmenu x7">
                            <span class="btop"></span><span class="bbot"></span>
                        </div>
                    </a>
                    <div class="accordion__content">
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                        </p>
                        <div class="BtnWrap">
                            <a href="" class="Button OrangeBtn">Apply Now</a>
                        </div>
                    </div>
                </li>
                <li class="accordion__item">
                    <a class="accordion__title" href="">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        <div class="bmenu x7">
                            <span class="btop"></span><span class="bbot"></span>
                        </div>
                    </a>
                    <div class="accordion__content">
                        <h3 class="Head">Heading Demo</h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent congue tincidunt tempor. Sed vitae neque libero. In efficitur ex eget nunc vestibulum, eu molestie metus condimentum. Integer non elit et orci scelerisque egestas. Suspendisse a semper nisl. Sed consequat velit ac aliquet facilisis. Donec molestie tellus id suscipit tincidunt. Mauris ac mauris urna. Quisque vitae eros sodales, tempor ex non, pellentesque quam. Etiam aliquet sem ac luctus venenatis. Sed faucibus velit in tellus ullamcorper, fringilla maximus purus tempus. Cras semper mauris eget fermentum porttitor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent congue tincidunt tempor. Sed vitae neque libero. In efficitur ex eget nunc vestibulum, eu molestie metus condimentum. Integer non elit et orci scelerisque egestas. Suspendisse a semper nisl. Sed consequat velit ac aliquet facilisis. Donec molestie tellus id suscipit tincidunt. Mauris ac mauris urna. Quisque vitae eros sodales, tempor ex non, pellentesque quam. Etiam aliquet sem ac luctus venenatis. Sed faucibus velit in tellus ullamcorper, fringilla maximus purus tempus. Cras semper mauris eget fermentum porttitor.
                        </p>
                        <h3 class="Head">Heading Demo</h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent congue tincidunt tempor. Sed vitae neque libero. In efficitur ex eget nunc vestibulum, eu molestie metus condimentum. Integer non elit et orci scelerisque egestas. Suspendisse a semper nisl. Sed consequat velit ac aliquet facilisis. Donec molestie tellus id suscipit tincidunt. Mauris ac mauris urna. Quisque vitae eros sodales.
                        </p>
                    </div>
                </li>
                <li class="accordion__item">
                    <a class="accordion__title" href="">Lorem ipsum dolor sit
                        <div class="bmenu x7">
                            <span class="btop"></span><span class="bbot"></span>
                        </div>
                    </a>
                    <div class="accordion__content">
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                        </p>
                        <div class="BtnWrap">
                            <a href="" class="Button OrangeBtn">Apply Now</a>
                        </div>
                    </div>
                </li>
            </ul>

        </div>
    </div>
</section>