
<?php
	include('template-parts/header.php');
?>

<section class="LeftContentRightImage Section greySection" id="aboutUs">
	<div class="container">
		<div class="row">
			<div class="col-md-6 order-2-md text-center-md">
				<div class="MainHeading">
					<h2>PURPOSEFUL <span>INNOVATION</span> & SUSTAINABILITY</h2>
				</div>
				<div class="contentWrap">
					<h3>Making an impact with innovative, sustainable, disinfection techniques</h3>
					<p>Our mission at PurplePure is to provide unique disinfection products for everyone, especially the brave health professionals who are battling the good fight for us all. With our combined experience of over 100 years in the disinfection and lighting business, we are uniquely placed to provide the best solutions to the world. Proudly made in India, our products pass rigorous testing and quality parameters to provide you with a safe, reliable solution for daily disinfection.</p>
					<div class="btn-group">
						<a href="#disinfectionScience">Explore the Science</a>
						<a href="#products">Browse Products</a>
					</div>
				</div>
			</div>
			<div class="col-md-6 mb-2-md">
				<div class="imgWrap">
					<img src="assets/img/tempimg/man-in-blue-scrubs.png" alt="innovation_image" width="572px" height="auto">
				</div>
				
			</div>
		</div>
	</div>
</section>

<section class="ContentWithBackground Section text-center-md">
	<div class="container">
		<div class="MainHeading">
			<h2>Disinfect your surroundings <span>with STERLIT T1</span></h2>
		</div>
		<p>The STERLIT T1 Disinfection lamp protects your surroundings with powerful <span>UVC light </span> and kills up to 99% of germs on surfaces, water and air.</p>
		<!-- <a href="#reportWrap" class="Button downloadReport">Download Safely Guidelines</a> -->
	</div>
</section>

<section class="RightImageLeftContent Section" id="disinfectionScience">
	<div class="container">
		<div class="row">
			<div class="col-md-6 text-center-md order-2-md">
				<div class="MainHeading">
					<h2>UVC DISINFECTION RESEARCH & SCIENCE</h2>
				</div>
				<p>Medical Professionals are well aware of the benefits and capabilities of UVC Disinfection. In the light of current Covid-19 Pandemic, it has become imperative that each one of us gains first hand knowledge of the benefits and risks of UVC Disinfection. </p>
				<p>We believe that in the coming few years, each household will boast a UVC Disinfection device that safeguards everything they own and bring into their house.</p>

				<!-- <div class="achievmentNumbers">
					<div class="row">
						<div class="col-md-6 mt-3">
							<h3>
								1,20,000
							</h3>
							<span>Number of patients that our facts represent</span>
						</div>
						<div class="col-md-6 mt-3">
							<h3>
								1 in every 2
							</h3>
							<span>People will be infected at a hospital</span>
						</div>
						<div class="col-md-6 mt-3">
							<h3>
								$91B
							</h3>
							<span>Will be spent on coronavirus research in 2020 alone</span>
						</div>
						<div class="col-md-6 mt-3">
							<h3>
								UVC
							</h3>
							<span>Is the only viable option available for safety</span>
						</div>
					</div>
			    </div> -->
			</div>

			<div class="col-md-6 pl-4 mb-2-md">
				<div class="pseudoWrap">
					<img src="assets/img/tempimg/UVC_disinfection.png" alt="UVC_disinfection_img" width="" height="">
				</div>
			</div>
		</div>
	</div>
</section>

<section class="LeftContentRightBoxes Section" id="applications">
	<div class="container">
		<div class="row text-center-md">
			<div class="col-md-5 mb-4-md">
				<div class="contentWrap">
					<h2>WHAT IS UVC DISINFECTION?</h2>
					<p>Ultraviolet (UV) light is a component of the electromagnetic spectrum that falls in the region between visible light and X-Rays. This invisible radiation includes the wavelength range of 100 nm to 400 nm. UV light can be further subdivided and categorized into four separate regions: 100 nm to 200 nm.</p>
				</div>
                 <hr>
				<div class="contentWrap">
					<h2>HOW DOES UVC DISINFECTION WORK?</h2>
					<p>As evident by multiple research studies and reports, absorption of UVC light by bacteria and viruses can lead to the rupture of their cell walls that ultimately leads to microbial death of the organism.</p>
				</div>
			</div>
			<div class="col-md-7">
				<ul>
					<li>
						<div class="boxWrap">
							<div class="svgWrap">
								<svg class="icon icon-uva">
									<use xlink:href="assets/img/cobold-sprite.svg#icon-uva"></use>
								</svg>
							</div>
							<h3>UVA</h3>
							<div>
								<h4>315 nm to 400 nm</h4>
								<p>Useful for printing, curing, lithography, sensing and medical applications</p>
							</div>
					    </div>
					</li>

					<li>
						<div class="boxWrap">
							<div class="svgWrap">
								<svg class="icon icon-uvb">
									<use xlink:href="assets/img/cobold-sprite.svg#icon-uvb"></use>
								</svg>
							</div>
							<h3>UVB</h3>
							<div>
								<h4>280 nm to 315 nm</h4>
								<p>Useful for curing, tanning and medical applications</p>
							</div>
					    </div>
					</li>

					<li>
						<div class="boxWrap">
							<div class="svgWrap">
								<svg class="icon icon-uvc">
									<use xlink:href="assets/img/cobold-sprite.svg#icon-uvc"></use>
								</svg>
							</div>
							<h3>UVC</h3>
							<div>
								<h4>200 nm to 280 nm</h4>
								<p>Useful for disinfection and sensing</p>
							</div>
					    </div>
					</li>

					<li>
						<div class="boxWrap">
							<div class="svgWrap">
								<svg class="icon icon-far_uv">
									<use xlink:href="assets/img/cobold-sprite.svg#icon-far_uv"></use>
								</svg>
							</div>
							<h3>Far UV</h3>
							<div>
								<h4>100 nm to 200 nm</h4>
								<p>These wavelenghts propogate only in vacuum</p>
							</div>
					    </div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>

<section class="ContentWithBackground leftInfoRightButton Section">
	<div class="container">
		<div class="row alignItemCenter">
			<div class="col-lg-6">
				<div class="MainHeading">
				<h2>Download our PDF Report on <span>UVC Disinfection for your home</span></h2>
				</div>
			</div>
			<div class="col-lg-6 text-right">
				<a href="#reportWrap" class="Button downloadReport">Download Report</a>
			</div>
		</div>	
	</div>
</section>

<section class="LeftSliderRightContent Section" id="products">
	<div class="container">
		<div class="MainHeading text-center-md">
			<h2>PRODUCTS</h2>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="sliderWrapper">
	               	<div class="productSlider">
	               		<div class="productWrap">
	               			<img src="assets/img/tempimg/product.png" alt="product_img" width="307px" height="auto">
	               		</div>
	               		<div class="productWrap">
	               			<img src="assets/img/tempimg/product.png" alt="product_img" width="307px" height="auto">
	               		</div>
	               		<div class="productWrap">
	               			<img src="assets/img/tempimg/product.png" alt="product_img" width="307px" height="auto">
	               		</div>
	               		<div class="productWrap">
	               			<img src="assets/img/tempimg/product.png" alt="product_img" width="307px" height="auto">
	               		</div>
	               	</div>
               	</div>
			</div>
			<div class="col-md-6">
				<div class="contentWrap">
					<div class="innerContent">
					<h3>STERLIT T1 <span>Disinfection Lamp</span></h3>
					<p>The STERLIT T1 Disinfection lamp will sterilize any surface with powerful UVC light & kills up to 99% of surface germs and bacteria. Safe to use with instructions and microwave sensors, it is now available at special Lockdown Prices. Kill 99% germs & bacteria. Safe to use at home.</p>
					</div>
					<a href="#" class="Button"><span>EXPLORE STERLITE T1</span> <span><img src="assets/img/right_arrow.svg" alt="arrow"></span></a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="TopImageBottomContent Section" id="investors">
	<div class="container">
		<div class="MainHeading">
			<h2>RESOURCES</h2>
			<p>Useful resources from our experts to keep you safe in this environment.</p>
		</div>
		<div class="SliderWrap">
			<div class="singleSlider">
				<a href="https://www.bbc.com/future/article/20200327-can-you-kill-coronavirus-with-uv-light" target="_blank">
					<img src="assets/img/tempimg/blog1.jpg" alt="blog_img" width="278px" height="207px">
					<p>There’s only one type of UV that can reliably inactivate Covid-19 – and it’s extremely dangerous.</p>
				</a>
			</div>
			<div class="singleSlider">
				<a href="https://www.infectioncontroltoday.com/environmental-hygiene/no-touch-technology-manufacturers-share-perspectives-key-issues" target="_blank">
					<img src="assets/img/tempimg/blog2.jpg" alt="blog_img" width="278px" height="207px">
					<p>No-Touch Technology Manufacturers Share Perspectives on Key Issues</p>
			    </a>
			</div>
			<div class="singleSlider">
				<a href="http://www.uvresources.com/blog/how-uv-c-lamps-can-combat-the-flu/" target="_blank">
					<img src="assets/img/tempimg/blog3.jpg" alt="blog_img" width="278px" height="207px">
					<p>How UV-C Lamps Can Combat the Flu</p>
				</a>
			</div>
			<div class="singleSlider">
				<a href="https://rduvc.com/wp-content/uploads/AJIC-A-model-for-choosing-an-automated-ultraviolet-C-disinfection-system-and-building-a-case-for-the-C-suite-Two-case-reports.pdf" target="_blank">
					<img src="assets/img/tempimg/resource2.png" alt="blog_img" width="278px" height="207px">
					<p>A model for choosing an automated UVC Disinfection System</p>
			    </a>
			</div>
			<div class="singleSlider">
				<a href="http://www.iuva.org/resources/covid-19/Advice-for-the-selection-and-operation-of-equipment-for-the-UV-disinfection-of-air-and-surfaces.pdf" target="_blank">
					<img src="assets/img/tempimg/resource1.png" alt="blog_img" width="278px" height="207px">
					<p>Advice for selection of equipment for UV Disinfection</p>
				</a>
			</div>
		</div>
	</div>
</section>

<section class="formSection Section" id="contactUs">
	<div class="container">
		<div class="MainHeading text-center">
			<h2>Join The <span>Waitlist</span></h2>
		</div>
		<div class="row">
			<div class="col-lg-6">
             <div class="ComingSoonWrap">
               <div id="timer">
				  <div id="days"></div>
				  <div id="hours"></div>
				  <div id="minutes"></div>
				  <div id="seconds"></div>
				</div>
             </div>	
			</div>
			<div class="col-lg-6">
				<div class="formWrapper">
					<form action="" method="post" class="contactform">
						<input type="text" placeholder="First Name*" name="fName">
						<input type="text" placeholder="Last Name" name="lName">
						<input type="text" placeholder="Phone Number*" name="Phone">
						<input type="email" placeholder="Email*" name="Email">
						<input type="text" placeholder="City*" name="City">
						<label for="message">Message</label>
						<textarea name="Message" id="message"></textarea>
						<input type="submit" value="Submit" id="submit">
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<?php 
	include('template-parts/footer.php');
?>
	